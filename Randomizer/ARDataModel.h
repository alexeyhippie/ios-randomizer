//
//  ARDataModel.h
//  Randomizer
//
//  Created by Alexey Hippie on 01/01/14.
//  Copyright (c) 2014 Alexey Hippie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARDataModel : NSObject

+ (ARDataModel *)sharedModel;

- (NSArray *)getItems;
- (void)addItem:(NSString *)item;
- (void)removeItem:(NSString *)item;
- (void)clearItemList;

- (NSString *)randomize;

- (void)save;
- (void)load;

@end
