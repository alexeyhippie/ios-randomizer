//
//  ARAppDelegate.h
//  Randomizer
//
//  Created by Alexey Hippie on 01/01/14.
//  Copyright (c) 2014 Alexey Hippie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
