//
//  ARDataModel.m
//  Randomizer
//
//  Created by Alexey Hippie on 01/01/14.
//  Copyright (c) 2014 Alexey Hippie. All rights reserved.
//

#import "ARDataModel.h"

static NSInteger const kLimit = 1000;

@interface ARDataModel ()

@property (nonatomic, strong) NSMutableArray *itemsArray;

@end

@implementation ARDataModel

+ (ARDataModel *)sharedModel {
    static dispatch_once_t pred;
    static ARDataModel *model = nil;
    dispatch_once(&pred, ^{
        model = [[ARDataModel alloc] init];
    });
    
    return model;
}

- (id)init {
    self = [super init];
    if (self) {
        self.itemsArray = [NSMutableArray array];
    }
    
    return self;
}

- (NSArray *)getItems {
    return [NSArray arrayWithArray:self.itemsArray];
}

- (void)addItem:(NSString *)item {
    if (item.length > 0) {
        [self.itemsArray addObject:item];
    }
}

- (void)removeItem:(NSString *)item {
    if (item.length > 0) {
        if ([self.itemsArray containsObject:item]) {
            [self.itemsArray removeObject:item];
        }
    }
}

- (void)clearItemList {
    self.itemsArray = [NSMutableArray array];
}

- (NSString *)randomize {
    NSString *result = @"";
    
    NSInteger count = self.itemsArray.count;
    NSMutableDictionary *scores = [NSMutableDictionary dictionary];
    for (NSString *item in self.itemsArray) {
        [scores setObject:@0 forKey:item];
    }
    
    while ([self _maxFromScores:scores] != kLimit) {
        NSInteger i = arc4random() % count;
        NSString *key = self.itemsArray[i];
        NSInteger value = [[scores objectForKey:key] integerValue] + 1;
        [scores setObject:[NSNumber numberWithInteger:value] forKey:key];
    }
    
    // results
    NSMutableString *tempresult = [NSMutableString string];
    NSArray *sorted = [scores keysSortedByValueUsingSelector:@selector(compare:)];
    for (int i = 0; i < count; i++) {
        NSString *item = sorted[count - 1 - i];
        [tempresult appendFormat:@"%i. %@ - %i \n", i+1, item, [[scores objectForKey:item] intValue]];
    }
    
    result = [NSString stringWithString:tempresult];
    
    return result;
}

#pragma mark - handle

- (NSInteger)_maxFromScores:(NSDictionary *)scores {
    NSInteger max = 0;
    
    for (NSNumber *score in scores.allValues) {
        if (score.integerValue > max) {
            max = score.integerValue;
        }
    }
    
    return max;
}

#pragma mark - save / load

- (void)save {
    
}

- (void)load {
    
}

@end
