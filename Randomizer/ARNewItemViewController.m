//
//  ARNewItemViewController.m
//  Randomizer
//
//  Created by Alexey Hippie on 01/01/14.
//  Copyright (c) 2014 Alexey Hippie. All rights reserved.
//

#import "ARNewItemViewController.h"
#import "ARDataModel.h"

@interface ARNewItemViewController ()

@property (nonatomic) IBOutlet UITextField *itemNameTextField;

- (IBAction)doneButtomTapped:(id)sender;
- (IBAction)cancelButtomTapped:(id)sender;

@end

@implementation ARNewItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.itemNameTextField becomeFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneButtomTapped:(id)sender {
    if (self.itemNameTextField.text.length > 0) {
        // save item
        [[ARDataModel sharedModel] addItem:self.itemNameTextField.text];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelButtomTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
