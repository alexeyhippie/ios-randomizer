//
//  main.m
//  Randomizer
//
//  Created by Alexey Hippie on 01/01/14.
//  Copyright (c) 2014 Alexey Hippie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ARAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ARAppDelegate class]));
    }
}
