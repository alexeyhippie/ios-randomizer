//
//  ARViewController.m
//  Randomizer
//
//  Created by Alexey Hippie on 01/01/14.
//  Copyright (c) 2014 Alexey Hippie. All rights reserved.
//

#import "ARHomeViewController.h"
#import "ARDataModel.h"

@interface ARHomeViewController ()

@property (nonatomic) IBOutlet UITableView *itemTable;

- (IBAction)randomize:(id)sender;

@property (nonatomic) NSArray *itemsArray;

@end

@implementation ARHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.itemsArray = [[ARDataModel sharedModel] getItems];
    [self.itemTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    count = self.itemsArray.count;
    
    return count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    
    cell.textLabel.text = self.itemsArray[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat h = 30.f;
    
    return h;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] init];
    header.alpha = 0.0;
    header.opaque = NO;
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.alpha = 0.0;
    view.opaque = NO;
    return view;
}



- (IBAction)randomize:(id)sender {
    NSString *randomized = [[ARDataModel sharedModel] randomize];
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Results"
                                                message:randomized
                                               delegate:nil
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil];
    [a show];
}

- (IBAction)clear:(id)sender {
    [[ARDataModel sharedModel] clearItemList];
    self.itemsArray = [[ARDataModel sharedModel] getItems];
    [self.itemTable reloadData];
}

@end
